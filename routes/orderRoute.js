const express = require('express');
const router = express.Router();
const auth = require('../auth')
const orderController = require('../controllers/orderController');





router.post('/createOrder', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		orderController.createOrder(userData.email, req.body).then(resultFromController => res.send(resultFromController))
	} 
	else {
		return "Admin cannot create an order"
	}

})

router.get('/getOrder', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		orderController.getOrder(userData.id).then(resultFromController => res.send(resultFromController))
	} 
	else {
		return "Admin cannot retrieve an order"
	}

})

router.get('/getAllOrder', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		return "Only Admin can retrieve all orders"
	} 
	else {
		orderController.getAllOrder().then(resultFromController => res.send(resultFromController))
	}

})

module.exports = router;
