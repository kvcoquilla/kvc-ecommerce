const express = require('express');
const router = express.Router();
const auth = require('../auth')
const prodController = require('../controllers/prodController');

router.post('/addProduct',auth.verify,(req,res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		return "Non-admin cannot add product"
	} 
	else {
		prodController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}
})

router.get('/getAllProduct',(req,res) => {
	prodController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

router.get('/:prodId/getProduct',(req,res) => {
	prodController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


// UPDATING A CERTAIN PRODUCT: It will first get all the product details using the getProduct(). Then, it will pass the product details to updateProduct().
router.put('/:prodId/updateProduct', auth.verify, async (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		return "Not authorized to update product."
	} else {

		let prodInfo = await prodController.getProduct(req.params);

		prodController.updateProduct(prodInfo,req.params,req.body).then(resultFromController => res.send(resultFromController));
	}
	
})

router.put('/:prodId/archiveProduct', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		return "Not authorized to archive product."
	} else {
		prodController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
	
})

module.exports = router;