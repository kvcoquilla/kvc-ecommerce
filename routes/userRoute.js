const express = require('express');
const router = express.Router();
const auth = require('../auth')
const userController = require('../controllers/userController');

router.post('/register',(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login',(req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.put('/:userId', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		return "Not authorized to update user to admin"
	} else {
		userController.updateToAdmin(req.params).then(resultFromController => res.send(resultFromController));
	}
	
})

module.exports = router;