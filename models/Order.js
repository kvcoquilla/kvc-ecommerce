const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	user: {
		type : String,
		required: [true, 'Name of customer is required']
	},

	product_name : {
		type : String
	},

	product_price : {
		type : Number
	},

	qty :{
		type : Number
	},

	totalAmount : {
		type : Number
	},

	purchasedOn : {
		type : Date,
		default: new Date
	}

});

module.exports = mongoose.model('Order', orderSchema);