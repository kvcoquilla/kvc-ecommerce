const Order =  require('../models/Order');
const User =  require('../models/User');
const Product =  require('../models/Product');
const Parameters =  require('../models/Parameters');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.createOrder = async (userInfo,reqBody) => {

	let userinfo =  await User.findOne({email : userInfo}).then((user,error) => {

			if(error) {
				return "Cannot fetch user information"
			} else {
				return user
			}
	});


	let productinfo = await Product.findById(reqBody.product_id).then((prod,error) => {

			if(error) {
				return "Cannot fetch product information"
			} else {
				return prod
			}
	});

	let newOrder = new Order({
		user : userinfo.firstName + ' ' + userinfo.lastName,
		product_name : productinfo.name,
		product_price : productinfo.price,
		qty : reqBody.qty,
		totalAmount : reqBody.qty * productinfo.price,
	});

	return newOrder.save().then((order,error) => {
		if(error) {
			return "Failed to save order"
		} else {
			return "Order created"
		}
	})

}


module.exports.getOrder = async (userId) => {
	let userinfo = await User.findById(userId).then(user => {
		
		let userName = user.firstName + ' ' + user.lastName;

		return userName;
	})

	return Order.find({user : userinfo}).then((order,error) => {
		return order
	})
}



module.exports.getAllOrder = () => {
	return Order.find({}).then(result => {
		return result
	})
}