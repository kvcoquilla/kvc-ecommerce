const Product =  require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.addProduct = (reqBody) => {
	newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return "Cannot add product."
		}

		else {
			return "New product added"
		}
	})
};

module.exports.getAllProduct = () => {
	return Product.find({isActive : true}).then(result => {
		return result
	})
};

module.exports.getProduct = (reqParam) => {
	
	return Product.findById(reqParam.prodId).then((result,error) => {

		if(result.isActive !== true){
			return "Product not found"
		} else {
			return result
		}
		
	})
};

// UPDATING A CERTAIN PRODUCT: Since we get all the current details of the product using the getProduct(), we no longer have to enter the same product details in order to update one specific column.

// This will allow the user to only input the fields with update and not the whole product details.
module.exports.updateProduct = async (prodInfo,reqParam,reqBody) => {
	
	let newName, newDescription,newPrice,newisActive;

	if(prodInfo.name !== reqBody.name){

		newName = reqBody.name;
	} else if (reqBody.name == undefined || prodInfo.name === reqBody.name){
		newName = prodInfo.name
	};

	if(prodInfo.description !== reqBody.description){

		newDescription = reqBody.description;
	} else if (reqBody.description == undefined || prodInfo.description === reqBody.description) {
		newDescription = prodInfo.description
	};

	if(prodInfo.price !== reqBody.price){

		newPrice = reqBody.price;
	} else if (reqBody.price == undefined || prodInfo.price === reqBody.price) {
		newPrice = prodInfo.price
	};

	if(prodInfo.isActive !== reqBody.isActive){

		newisActive = reqBody.isActive
	} else if (reqBody.isActive == undefined || prodInfo.isActive === reqBody.isActive) {
		newisActive = prodInfo.isActive
	};


	let newProdDetails = {
		 name: newName,
		 description : newDescription,
		 price : newPrice,
		 isActive : newisActive
	}

	return Product.findByIdAndUpdate(reqParam.prodId,newProdDetails).then((prod,error) => {
		if (error) {
			return "Failed to update product"
		} else {
			return "Sucessfully updated product details."
		}
	})

}

module.exports.archiveProduct = (reqParam,reqBody) => {
	return Product.findByIdAndUpdate(reqParam.prodId, ({isActive : false})).then((user,error)=>{

		if(error){
			return "Failed to archive product"
		} else {
			return "Successfully archived product"
		}

	})
}