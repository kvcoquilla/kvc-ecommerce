const User =  require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password,10)
	});

	return newUser.save().then((user,error) => {
		if(error) {
			return "Cannot add user. Check details"
		}

		else {
			return "Successfully created new user."
		}
	})
};


module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null) {
			return "User doesn't exists"
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {token : auth.createAccessToken(result)}
			}

			else {
				return "Password do not match"
			}
		}
	})

}

module.exports.updateToAdmin = (reqParam,reqBody) => {
	return User.findByIdAndUpdate(reqParam.userId, ({isAdmin : true})).then((user,error)=>{

		if(error){
			return "Failed to update user as Admin"
		} else {
			return "Successfully updated user to Admin"
		}

	})
}